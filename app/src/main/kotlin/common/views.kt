package common

import android.view.View
import android.view.ViewTreeObserver

/**
 * Created by afeozzz on 20/12/16.
 */
inline fun View.waitForLayout(crossinline f: () -> Unit) = with(viewTreeObserver) {
    viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            if (measuredWidth > 0 && measuredHeight > 0) {
                viewTreeObserver.removeOnGlobalLayoutListener(this)
                f()
            }
        }
    })
}