package common

import android.content.Context
import android.util.DisplayMetrics
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.content.Context.INPUT_METHOD_SERVICE
import android.app.Activity
import android.view.View


/**
 * Created by afeozzz on 20/12/16.
 */
fun Context.showKeyboard() {
    val input = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
    input.toggleSoftInput(InputMethodManager.SHOW_FORCED,
            InputMethodManager.HIDE_IMPLICIT_ONLY)
}

fun Context.hideKeyboard() {
    val input = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    input.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
}

fun Context.convertDpToPixel(dp: Float) : Float {
    val metrics = resources.displayMetrics
    val px = dp * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    return px
}