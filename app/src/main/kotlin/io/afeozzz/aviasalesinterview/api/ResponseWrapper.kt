package io.afeozzz.aviasalesinterview.api

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import io.afeozzz.aviasalesinterview.data.City

/**
 * Created by afeozzz on 20/12/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class ResponseWrapper (@JsonProperty("cities") val cities: List<City>)