package io.afeozzz.aviasalesinterview.api

import io.afeozzz.aviasalesinterview.data.City
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.OkHttpClient
import javax.xml.datatype.DatatypeConstants.SECONDS
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable
import rx.Single
import java.util.concurrent.TimeUnit


/**
 * Created by afeozzz on 20/12/16.
 */

class FlightApiService {

    interface Api {
        @GET("autocomplete")
        fun searchCity(@Query("term") query: String, @Query("lang") lang: String): Observable<ResponseWrapper>
    }

    private val API_ENDPOINT = "https://yasen.hotellook.com/"

     fun createInstance(): Api {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val okClientBuilder = OkHttpClient.Builder()
        okClientBuilder.connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)


        okClientBuilder.interceptors().add(logging)

        val okHttpClient = okClientBuilder.build()

        val retrofit = Retrofit.Builder()
                .baseUrl(API_ENDPOINT)
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .build()

        val api = retrofit.create(Api::class.java)
        return api
    }

}