package io.afeozzz.aviasalesinterview.data

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

/**
 * Created by afeozzz on 19/12/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class City(@JsonProperty("latinCity") val latinCity: String,
           @JsonProperty("latinFullName") val latinFullName: String,
           @JsonProperty("iata") val iata: List<String>,
           @JsonProperty("location") val location: Location) : Serializable