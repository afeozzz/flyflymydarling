package io.afeozzz.aviasalesinterview.data

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

/**
 * Created by afeozzz on 20/12/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class Location(@JsonProperty("lon") val lon: Double, @JsonProperty("lat") val lat: Double) : Serializable