package io.afeozzz.aviasalesinterview.plane_flight

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Color
import android.graphics.Path
import android.graphics.PathMeasure
import android.graphics.Point
import android.support.v4.content.ContextCompat
import android.view.animation.AccelerateDecelerateInterpolator
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.Projection
import com.google.android.gms.maps.model.*
import io.afeozzz.aviasalesinterview.R
import common.convertDpToPixel
import io.afeozzz.aviasalesinterview.data.City
import java.util.*

/**
 * Created by afeozzz on 20/12/16.
 */
class FlyManager(val googleMap: GoogleMap, val ctx: Context) {

    lateinit var positionStart: LatLng
    lateinit var positionEnd: LatLng
    lateinit var pathMeasure: PathMeasure
    lateinit var planeMarker: Marker
    lateinit var planeCoordinatesList: ArrayList<Any>
    lateinit var projection: Projection

    var planeAnimator: ValueAnimator? = null
    var progress: Float = 0.0f
    var planeAdded = false
    var circleOffset: Float

    init {
        circleOffset = ctx.convertDpToPixel(3F)
    }

    companion object {
        val MAX_STEPS = 100
    }

    fun setRouteMarkers(startCity: City, endCity: City) {
        positionStart = LatLng(startCity.location.lat, startCity.location.lon)
        positionEnd = LatLng(endCity.location.lat, endCity.location.lon)

        addCityMarker(startCity, endCity)
    }

    fun addCityMarker(startCity: City, endCity: City) {

        val cityMarker = CityMarker(ctx)
        cityMarker.setIata(startCity.iata[0])
        val m1 = MarkerOptions().position(LatLng(startCity.location.lat, startCity.location.lon)).icon(BitmapDescriptorFactory.fromBitmap(cityMarker.getBitmap()))
        cityMarker.isDrawingCacheEnabled = false
        cityMarker.setIata(endCity.iata[0])
        val m2 = MarkerOptions().position(LatLng(endCity.location.lat, endCity.location.lon)).icon(BitmapDescriptorFactory.fromBitmap(cityMarker.getBitmap()))


        googleMap.addMarker(m1)
        googleMap.addMarker(m2)
    }


    fun drawRoute() {
        projection = googleMap.projection
        val zoom = googleMap.cameraPosition.zoom

        val screenPointStart = projection.toScreenLocation(positionStart)
        val screenPointEnd = projection.toScreenLocation(positionEnd)

        val point = Point()
        pathMeasure = PathMeasure(buildBezierPath(screenPointStart, screenPointEnd), false)

        buildCoordinatesForPlane()

        val positionInPoint = FloatArray(2)
        val circleOptions = CircleOptions()

        var steps = (pathMeasure.length / (zoom + circleOffset)).toInt()

        if (steps > MAX_STEPS) {
            steps = MAX_STEPS
        }

        val stepDistance = (pathMeasure.length - zoom * steps) / steps


        for (i in 0..steps) {
            val currentPosition = (zoom + stepDistance * i) * (zoom + stepDistance) / 2

            pathMeasure.getPosTan(currentPosition, positionInPoint, null)
            point.x = positionInPoint[0].toInt()
            point.y = positionInPoint[1].toInt()

            circleOptions.radius((1000 * (100 / zoom)).toDouble())
            circleOptions.visible(true)
            circleOptions.strokeWidth(0f)
            circleOptions.fillColor(ContextCompat.getColor(ctx, R.color.transparent_black))
            val latLng = projection.fromScreenLocation(point)
            circleOptions.center(latLng)

            googleMap.addCircle(circleOptions)
        }

        startAnimationPlane()
    }

    private fun buildCoordinatesForPlane() {
        planeCoordinatesList = ArrayList()
        val pathLength = pathMeasure.length
        val position = FloatArray(2)

        for (i in 0..MAX_STEPS) {
            pathMeasure.getPosTan(pathLength / MAX_STEPS * i, position, null)
            planeCoordinatesList.add(projection.fromScreenLocation(Point(position[0].toInt(), position[1].toInt())))
        }
    }

    fun buildBezierPath(startPosition: Point, endPosition: Point): Path {
        val path = Path()
        val centerPoint = Point()
        centerPoint.set((startPosition.x + endPosition.x) / 2, (startPosition.y + endPosition.y) / 2)
        path.moveTo(startPosition.x.toFloat(), startPosition.y.toFloat())

        val firstBezierPoint = Point()
        val secondBezierPoint = Point()

        val pointsOne = computeBezier(centerPoint, startPosition, Math.cos(Math.toRadians(290.0)), Math.sin(Math.toRadians(290.0)))
        val pointsTwo = computeBezier(endPosition, centerPoint, Math.cos(Math.toRadians(50.0)), Math.sin(Math.toRadians(50.0)))

        firstBezierPoint.set(pointsOne[0], pointsOne[1])
        secondBezierPoint.set(pointsTwo[0], pointsTwo[1])

        path.cubicTo(firstBezierPoint.x.toFloat(), firstBezierPoint.y.toFloat(), secondBezierPoint.x.toFloat(), secondBezierPoint.y.toFloat(), endPosition.x.toFloat(), endPosition.y.toFloat())

        return path
    }

    fun placePlane(progress: Float) {

        val rotations = FloatArray(2)
        pathMeasure.getPosTan(pathMeasure.length * progress, null, rotations)
        if (!planeAdded) {
            planeMarker = googleMap.addMarker(
                    MarkerOptions().position(positionStart)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.plane))
                            .anchor(0.5f, 0.5f)
            )
            planeAdded = true
        }

        planeMarker.rotation = Math.toDegrees(Math.atan2(rotations[1].toDouble(), rotations[0].toDouble())).toFloat()

        val planeCoordinates: LatLng
        val size = planeCoordinatesList.size
        val progressIndex = (((size - 1)) * progress).toInt()

        val distance = (MAX_STEPS * progress) - progressIndex

        if (progressIndex != size - 1) {
            val planeFirstPosition = planeCoordinatesList[progressIndex] as LatLng
            val planeNextPosition = planeCoordinatesList[progressIndex + 1] as LatLng

            planeCoordinates = LatLng(computeCoordinates(planeFirstPosition.latitude, planeNextPosition.latitude, distance),
                    computeCoordinates(planeFirstPosition.longitude, planeNextPosition.longitude, distance))
        } else {
            planeCoordinates = planeCoordinatesList[size - 1] as LatLng

        }

        planeMarker.position = planeCoordinates
    }

    fun startAnimationPlane() {
        stopAnimation()

        planeAnimator = ValueAnimator.ofFloat(progress, 1f)
        planeAnimator?.interpolator = AccelerateDecelerateInterpolator()
        planeAnimator?.addUpdateListener { p0 -> placePlane(p0.animatedValue as Float) }
        planeAnimator?.duration = 20000
        planeAnimator?.start()
    }

    fun stopAnimation() {
        if (planeAnimator != null) {
            planeAnimator?.cancel()
        }
    }

    fun clear() {
        googleMap.clear()
        planeAdded = false
    }

    fun computeCoordinates(val1: Double, val2: Double, distance: Float): Double {
        return val1 + ((val2 - val1) * distance)
    }

    fun computeBezier(p1: Point, p2: Point, angle1: Double, angle2: Double): IntArray {

        val pointControl1 = ((p1.x - p2.x) * angle1 - (p1.y - p2.y) * angle2 + p2.x).toInt()

        val pointControl2 = ((p1.x - p2.x) * angle2 - (p1.y - p2.y) * angle1 + p2.y).toInt()

        return intArrayOf(pointControl1, pointControl2)
    }

}