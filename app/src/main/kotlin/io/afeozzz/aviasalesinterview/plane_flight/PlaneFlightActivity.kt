package io.afeozzz.aviasalesinterview.plane_flight

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import io.afeozzz.aviasalesinterview.Constants
import io.afeozzz.aviasalesinterview.R
import io.afeozzz.aviasalesinterview.data.City
import common.waitForLayout

/**
 * Created by afeozzz on 19/12/16.
 */
class PlaneFlightActivity : AppCompatActivity(), OnMapReadyCallback, FlyView {

    lateinit var mapFragment: SupportMapFragment
    lateinit var presenter: PlaneFlightPresenter

    lateinit var startCity: City
    lateinit var endCity: City


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.plane_flight_activity_layout)

        startCity = intent.extras.get(Constants.START_CITY_TAG) as City
        endCity = intent.extras.get(Constants.END_CITY_TAG) as City

        presenter = PlaneFlightPresenter()
        presenter.attachView(this)

        mapFragment = supportFragmentManager.findFragmentById(R.id.maps) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }


    override fun onMapReady(gm: GoogleMap?) {

        if (gm != null) {
            presenter.onMapReady(gm, this)

            mapFragment.view?.waitForLayout {
                presenter.init()
            }
        }
    }

    override fun removeMarkers(flyManager: FlyManager) {
        flyManager.clear()
    }

    override fun showMarkers(flyManager: FlyManager) {
        flyManager.setRouteMarkers(startCity, endCity)
    }

    override fun drawRoute(flyManager: FlyManager) {
        flyManager.drawRoute()
    }

    override fun showFlyAnimation(flyManager: FlyManager) {
        flyManager.startAnimationPlane()
    }

    override fun onPause() {
        super.onPause()
        presenter.detachView()
    }

    override fun onResume() {
        super.onResume()
        if (presenter.view == null) {
            presenter.attachView(this)
        }
    }
}