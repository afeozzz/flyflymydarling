package io.afeozzz.aviasalesinterview.plane_flight

import io.afeozzz.aviasalesinterview.base.BasePresenter

/**
 * Created by afeozzz on 20/12/16.
 */
interface FlyView : BasePresenter.MvpView {
    fun showMarkers(flyManager: FlyManager)
    fun removeMarkers(flyManager: FlyManager)
    fun drawRoute(flyManager: FlyManager)
    fun showFlyAnimation(flyManager: FlyManager)
}