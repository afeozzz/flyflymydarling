package io.afeozzz.aviasalesinterview.plane_flight

import android.content.Context
import android.util.Log
import com.google.android.gms.maps.GoogleMap
import io.afeozzz.aviasalesinterview.base.Presenter
import io.afeozzz.aviasalesinterview.data.City

/**
 * Created by afeozzz on 20/12/16.
 */
class PlaneFlightPresenter() : Presenter<FlyView>() {

    lateinit var flyManager: FlyManager
    var zoom: Float = 0.0f

    fun onMapReady(googleMap: GoogleMap, ctx: Context) {
        flyManager = FlyManager(googleMap, ctx)

        googleMap.setOnCameraIdleListener {
            val newZoom = googleMap.cameraPosition.zoom

            if (zoom != newZoom) {
                view().removeMarkers(flyManager)
                zoom = newZoom
                init()
            }
        }
    }

    fun init() {
        view().showMarkers(flyManager)
        view().drawRoute(flyManager)
    }

}