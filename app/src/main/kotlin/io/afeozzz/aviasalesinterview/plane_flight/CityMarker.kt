package io.afeozzz.aviasalesinterview.plane_flight

import android.content.Context
import android.graphics.Bitmap
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.TextView
import io.afeozzz.aviasalesinterview.R

/**
 * Created by afeozzz on 19/12/16.
 */
class CityMarker : FrameLayout {

    var iataTextView: TextView

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    init {
        LayoutInflater.from(context).inflate(R.layout.city_marker_layout, this, true)
        iataTextView = findViewById(R.id.iata_textview) as TextView
    }


    fun getBitmap(): Bitmap {
        isDrawingCacheEnabled = true
        measure(MeasureSpec.makeMeasureSpec(0, 0), MeasureSpec.makeMeasureSpec(0, 0))
        layout(0, 0, measuredWidth, measuredHeight)
        buildDrawingCache()
        return drawingCache
    }

    fun setIata(iata: String) {
        iataTextView.text = iata
    }

}