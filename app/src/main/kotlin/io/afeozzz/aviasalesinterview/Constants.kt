package io.afeozzz.aviasalesinterview

/**
 * Created by afeozzz on 20/12/16.
 */
object Constants {
    val CITY_TAG = "city"
    val REQUEST_CODE_FOR_START_CITY_SEARCH = 42
    val REQUEST_CODE_FOR_END_CITY_SEARCH = 88
    val START_CITY_TAG = "start_city"
    val END_CITY_TAG = "end_city"
}