package io.afeozzz.aviasalesinterview.search

import io.afeozzz.aviasalesinterview.base.BasePresenter
import io.afeozzz.aviasalesinterview.data.City

/**
 * Created by afeozzz on 20/12/16.
 */
interface SearchView : BasePresenter.MvpView{
    fun onStartCitySelected(city: City)
    fun onEndCitySelected(city: City)
}