package io.afeozzz.aviasalesinterview.search

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import android.widget.Toast
import io.afeozzz.aviasalesinterview.Constants
import io.afeozzz.aviasalesinterview.R
import io.afeozzz.aviasalesinterview.data.City
import io.afeozzz.aviasalesinterview.plane_flight.PlaneFlightActivity
import io.afeozzz.aviasalesinterview.select_cities.SelectCityActivity

/**
 * Created by afeozzz on 19/12/16.
 */
class SearchActivity : AppCompatActivity(), SearchViewAction, SearchView {

    lateinit var presenter: SearchPresenter
    lateinit var startCityTextView: TextView
    lateinit var endCityTextView: TextView
    lateinit var airportStartTextView: TextView
    lateinit var airportEndTextView: TextView

    var startCitySelected = false
    var endCitySelected = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.search_activity_layout)

        findViewById(R.id.select_start_city_layout).setOnClickListener { startCitySelectButtonClicked() }
        findViewById(R.id.select_end_city_layout).setOnClickListener { endCitySelectButtonClicked() }
        findViewById(R.id.findTicketsButton).setOnClickListener { searchButtonClicked() }

        startCityTextView = findViewById(R.id.start_city_text_view) as TextView
        endCityTextView = findViewById(R.id.end_city_text_view) as TextView
        airportStartTextView = findViewById(R.id.airport_start) as TextView
        airportEndTextView = findViewById(R.id.airport_end) as TextView

        presenter = SearchPresenter()
        presenter.attachView(this)
    }

    override fun endCitySelectButtonClicked() {
        startActivityForResult(Intent(this, SelectCityActivity::class.java), Constants.REQUEST_CODE_FOR_END_CITY_SEARCH)
    }

    override fun searchButtonClicked() {
        if (endCitySelected && startCitySelected) {
            val intent = Intent(this, PlaneFlightActivity::class.java)
            intent.putExtra(Constants.START_CITY_TAG, presenter.getStartCity)
            intent.putExtra(Constants.END_CITY_TAG, presenter.getEndCity)

            startActivity(intent)
        }else {
            Toast.makeText(this, "You have to choose a both cities", Toast.LENGTH_SHORT).show()
        }
    }

    override fun startCitySelectButtonClicked() {
        startActivityForResult(Intent(this, SelectCityActivity::class.java), Constants.REQUEST_CODE_FOR_START_CITY_SEARCH)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (presenter.view == null) {
            presenter.attachView(this)
        }

        presenter.onActivityResult(requestCode, resultCode, data)
    }

    override fun onEndCitySelected(city: City) {
        endCityTextView.text = city.latinCity
        airportEndTextView.text = city.latinFullName
        endCitySelected = true
    }

    override fun onStartCitySelected(city: City) {
        startCityTextView.text = city.latinCity
        airportStartTextView.text = city.latinFullName
        startCitySelected = true
    }

    override fun onResume() {
        if (presenter.view == null) {
            presenter.attachView(this)
        }
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
        presenter.detachView()
    }
}