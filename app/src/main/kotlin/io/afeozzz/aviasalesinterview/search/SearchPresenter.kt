package io.afeozzz.aviasalesinterview.search

import android.app.Activity
import android.content.Intent
import io.afeozzz.aviasalesinterview.Constants
import io.afeozzz.aviasalesinterview.base.Presenter
import io.afeozzz.aviasalesinterview.data.City
import java.util.*

/**
 * Created by afeozzz on 19/12/16.
 */
class SearchPresenter : Presenter<SearchView>() {

    lateinit var startCity: City
    lateinit var endCity: City

    val getStartCity : City get() = startCity
    val getEndCity : City get() = endCity


    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when(requestCode) {
                Constants.REQUEST_CODE_FOR_START_CITY_SEARCH -> {
                    startCity = data?.extras?.get(Constants.CITY_TAG) as City
                    view().onStartCitySelected(startCity)
                }
                Constants.REQUEST_CODE_FOR_END_CITY_SEARCH -> {
                    endCity = data?.extras?.get(Constants.CITY_TAG) as City
                    view().onEndCitySelected(endCity)
                }
            }
        }
    }
}