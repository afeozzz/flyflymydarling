package io.afeozzz.aviasalesinterview.search

import android.content.Intent
import io.afeozzz.aviasalesinterview.base.BasePresenter

/**
 * Created by afeozzz on 19/12/16.
 */
interface SearchViewAction : BasePresenter.MvpViewAction{
    fun startCitySelectButtonClicked()
    fun endCitySelectButtonClicked()
    fun searchButtonClicked()
}