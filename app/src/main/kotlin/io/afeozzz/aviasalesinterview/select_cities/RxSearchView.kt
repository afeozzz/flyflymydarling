package io.afeozzz.aviasalesinterview.select_cities

import android.support.annotation.NonNull
import android.support.v7.widget.SearchView
import android.support.v7.widget.SearchView.OnQueryTextListener
import rx.Observable
import rx.Subscriber
import rx.android.MainThreadSubscription
import android.support.v4.widget.SearchViewCompat.setOnQueryTextListener
import android.support.v4.widget.SearchViewCompat.getQuery




/**
 * Created by afeozzz on 20/12/16.
 */
object RxSearchView {
    fun queryTextChanges(view: SearchView): Observable<CharSequence> {
        return Observable.create(SearchViewQueryTextChangesOnSubscribe(view))
    }


    class SearchViewQueryTextChangesOnSubscribe(val view: SearchView) : Observable.OnSubscribe<CharSequence> {

        override fun call(t: Subscriber<in CharSequence>) {
            val watcher = object : OnQueryTextListener {
                override fun onQueryTextChange(s: String): Boolean {
                    if (!t.isUnsubscribed) {
                        t.onNext(s)
                        return true
                    }
                    return false
                }

                override fun onQueryTextSubmit(query: String): Boolean {
                    return false
                }
            }

            t.add(object : MainThreadSubscription() {
                override fun onUnsubscribe() {
                    view.setOnQueryTextListener(null)
                }
            })

            view.setOnQueryTextListener(watcher)

            t.onNext(view.query)
        }

    }
}