package io.afeozzz.aviasalesinterview.select_cities

import android.util.Log
import io.afeozzz.aviasalesinterview.Singletons
import io.afeozzz.aviasalesinterview.api.FlightApiService
import io.afeozzz.aviasalesinterview.base.Presenter
import io.afeozzz.aviasalesinterview.data.City
import rx.Scheduler
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.functions.Action1
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 * Created by afeozzz on 19/12/16.
 */
class SelectCityPresenter : Presenter<SelectCityView>() {

    fun searchCity(query: String) {
        view().showProgress()
        addSubscriber(
                Singletons.apiClient.searchCity(query, "ru")
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { response -> view().showContent(response.cities)},
                                { error -> view().showError(error) }
                        )
        )
    }

    fun onCityChoosed(): OnCitySearchListener {
        return object : OnCitySearchListener {
            override fun onCityFound(city: City) {
                view().onCityClicked(city)
            }
        }
    }

}