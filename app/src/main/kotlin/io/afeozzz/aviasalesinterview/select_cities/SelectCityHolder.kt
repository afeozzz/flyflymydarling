package io.afeozzz.aviasalesinterview.select_cities

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import io.afeozzz.aviasalesinterview.R
import io.afeozzz.aviasalesinterview.data.City
import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION
import android.os.Build
import android.util.TypedValue



/**
 * Created by afeozzz on 20/12/16.
 */
class SelectCityHolder(val view: View, val searchListener: OnCitySearchListener?) : RecyclerView.ViewHolder(view), View.OnClickListener {

    override fun onClick(p0: View?) {
        searchListener?.onCityFound(p0?.tag as City)
    }

    var iataTextVew: TextView
    var cityTextView: TextView
    var airPortTextView: TextView

    init {
        iataTextVew = view.findViewById(R.id.iata_textview) as TextView
        cityTextView = view.findViewById(R.id.city_text_view) as TextView
        airPortTextView = view.findViewById(R.id.airport_start) as TextView

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            val outValue = TypedValue()
            view.context.theme.resolveAttribute(android.R.attr.selectableItemBackground, outValue, true)
            view.setBackgroundResource(outValue.resourceId)
        }

        view.setOnClickListener (this)
    }

    fun bind (city: City) {

        airPortTextView.text = city.latinFullName
        cityTextView.text = city.latinCity

        if (city.iata.isNotEmpty()) {
            iataTextVew.text = city.iata[0]
            iataTextVew.visibility = View.VISIBLE
        }else {
            iataTextVew.visibility = View.GONE
        }


        view.tag = city
    }
}