package io.afeozzz.aviasalesinterview.select_cities

import io.afeozzz.aviasalesinterview.data.City

/**
 * Created by afeozzz on 20/12/16.
 */
interface OnCitySearchListener {
    fun onCityFound(city: City)
}