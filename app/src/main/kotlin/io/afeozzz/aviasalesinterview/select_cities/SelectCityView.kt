package io.afeozzz.aviasalesinterview.select_cities

import io.afeozzz.aviasalesinterview.base.BaseMvpLceView
import io.afeozzz.aviasalesinterview.data.City

/**
 * Created by afeozzz on 19/12/16.
 */
interface SelectCityView : BaseMvpLceView <List<City>> {
    fun onCityClicked(city: City)
}