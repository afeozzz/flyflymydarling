package io.afeozzz.aviasalesinterview.select_cities

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import io.afeozzz.aviasalesinterview.R
import io.afeozzz.aviasalesinterview.data.City
import java.util.*
import rx.subjects.PublishSubject



/**
 * Created by afeozzz on 20/12/16.
 */
class SelectCityAdapter : RecyclerView.Adapter<SelectCityHolder>() {

    var cities: ArrayList<City>

    var searchListener: OnCitySearchListener? = null

    init {
        cities = ArrayList<City>()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): SelectCityHolder {
        return SelectCityHolder(LayoutInflater.from(parent?.context).inflate(R.layout.select_city_item_layout, parent, false), searchListener)
    }

    override fun onBindViewHolder(holder: SelectCityHolder?, position: Int) {
        holder?.bind(cities[position])
    }

    override fun getItemCount(): Int {
        return cities.size
    }

    fun addItems(items: List<City>) {
        cities.clear()
        cities.addAll(items)
    }

}