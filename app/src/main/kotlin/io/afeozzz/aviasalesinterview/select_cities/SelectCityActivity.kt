package io.afeozzz.aviasalesinterview.select_cities

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import io.afeozzz.aviasalesinterview.Constants
import io.afeozzz.aviasalesinterview.R
import io.afeozzz.aviasalesinterview.data.City
import common.hideKeyboard
import io.afeozzz.aviasalesinterview.search.SearchActivity
import common.showKeyboard
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit


class SelectCityActivity : AppCompatActivity(), SelectCityView {

    lateinit var presenter: SelectCityPresenter
    lateinit var errorTextView: TextView
    lateinit var recyclerView: RecyclerView
    lateinit var progressBar: ProgressBar


    lateinit var adapter: SelectCityAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_city)

        val toolbar = findViewById(R.id.toolbar_layout) as Toolbar
        toolbar.setTitleTextColor(Color.WHITE)
        setSupportActionBar(toolbar)

        presenter = SelectCityPresenter()
        presenter.attachView(this)

        errorTextView = findViewById(R.id.error_text) as TextView
        recyclerView = findViewById(R.id.recycler_view) as RecyclerView
        progressBar = findViewById(R.id.progress_bar) as ProgressBar

        adapter = SelectCityAdapter()
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        adapter.searchListener = presenter.onCityChoosed()
    }

    override fun onPause() {
        super.onPause()
        presenter.detachView()
    }

    override fun showError(throwable: Throwable) {
        progressBar.visibility = View.GONE
        recyclerView.visibility = View.GONE
        errorTextView.visibility = View.VISIBLE
        errorTextView.text = throwable.message
    }

    override fun showProgress() {
        progressBar.visibility = View.VISIBLE
        recyclerView.visibility = View.GONE
        errorTextView.visibility = View.GONE
    }

    override fun showContent(data: List<City>) {
        progressBar.visibility = View.GONE
        recyclerView.visibility = View.VISIBLE
        errorTextView.visibility = View.GONE

        adapter.addItems(data)
    }

    override fun onCityClicked(city: City) {

        hideKeyboard()

        val bundle = Bundle()
        bundle.putSerializable(Constants.CITY_TAG, city)

        val intent = Intent(this, SearchActivity::class.java)
        intent.putExtras(bundle)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        val searchView = MenuItemCompat.getActionView(menu?.findItem(R.id.action_search)) as SearchView
        val searchMenuItem = menu?.findItem(R.id.action_search)
        MenuItemCompat.expandActionView(searchMenuItem)

        searchView.setIconifiedByDefault(false)

        RxSearchView.queryTextChanges(searchView)
                .debounce(200, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { query -> presenter.searchCity(query.toString()) },
                        { error -> showError(error) }
                )

        searchView.postDelayed({
            searchView.requestFocus()
            this.showKeyboard()
        }, 250)

        return true
    }

    override fun onResume() {
        super.onResume()
        if (presenter.view == null) {
            presenter.attachView(this)
        }
    }

}
