package io.afeozzz.aviasalesinterview

import io.afeozzz.aviasalesinterview.api.FlightApiService

/**
 * Created by afeozzz on 20/12/16.
 */
object Singletons {
    val apiClient by lazy {
        FlightApiService().createInstance()
    }
}