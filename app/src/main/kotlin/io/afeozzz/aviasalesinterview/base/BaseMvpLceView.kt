package io.afeozzz.aviasalesinterview.base

/**
 * Created by afeozzz on 19/12/16.
 */
interface BaseMvpLceView<in M : Any> : BasePresenter.MvpView {
    fun showError(throwable: Throwable)
    fun showProgress()
    fun showContent(data: M)
}