package io.afeozzz.aviasalesinterview.base

import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import rx.subscriptions.CompositeSubscription
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import rx.Subscription








/**
 * Created by afeozzz on 19/12/16.
 */
open class Presenter<T: BasePresenter.MvpView> : BasePresenter<T> {

    private val compositeSubscription = CompositeSubscription()


    @Volatile
    var view : T? = null


    fun view(): T {
        return view!!
    }

    override fun attachView(view: T) {
        this.view = view
    }

    override fun detachView() {
        view = null
        compositeSubscription.clear()
    }

    fun addSubscriber(subscription: Subscription) {
        compositeSubscription.add(subscription)
    }

}