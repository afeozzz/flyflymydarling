package io.afeozzz.aviasalesinterview.base

import android.icu.lang.UCharacter.GraphemeClusterBreak.T



/**
 * Created by afeozzz on 19/12/16.
 */
interface BasePresenter<in T: BasePresenter.MvpView> {

    fun attachView(view: T)

    fun detachView()

    interface MvpView

    interface MvpViewAction
}